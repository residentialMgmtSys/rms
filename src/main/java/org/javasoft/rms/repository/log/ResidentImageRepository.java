/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.repository.log;

import javax.transaction.Transactional;
import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.javasoft.rms.entity.image.ResidentImage;

/**
 *
 * @author ayojava
 */
@Repository
public interface ResidentImageRepository extends FullEntityRepository<ResidentImage, Long>{
    
    public ResidentImage findByProfileIdAndResidentId(Long profileId, Long residentId);
}
