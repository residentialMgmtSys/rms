/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.repository.log;

import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.javasoft.rms.entity.log.UserIdentityLog;

/**
 *
 * @author ayojava
 */
@Repository
//@Transactional
public interface UserIdentityLogRepository extends FullEntityRepository<UserIdentityLog, Long>{
    
}
