/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms;

/**
 *
 * @author ayojava
 */
public interface MessageIntf {
    
    void displayMessage(String template, String... args);
    
    void displayMessage(String template);
}
