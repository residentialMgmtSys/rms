/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.proxy.core;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static org.javasoft.rmsLib.api.util.InitDTOPath.START_UP_API;
import static org.javasoft.rmsLib.api.util.InitDTOPath.V1_API;
import org.javasoft.rmsLib.dto.util.InitDTO;

/**
 *
 * @author ayojava
 */
@Path(V1_API)
public interface InitProxy {

    @GET
    @Path(START_UP_API)
    @Produces({ MediaType.APPLICATION_JSON})
    public InitDTO loadInitDTO();
}
