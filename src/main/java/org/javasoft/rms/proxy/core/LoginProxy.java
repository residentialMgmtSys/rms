/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.proxy.core;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.LOGIN_API;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.V1_API;

/**
 *
 * @author ayojava
 */
@Path(V1_API)
public interface LoginProxy {
    
    @GET
    @Path(LOGIN_API)
    @Produces({ MediaType.APPLICATION_JSON})
    public Response logIn(@PathParam("userName") String userName,@PathParam("password") String password);
}
