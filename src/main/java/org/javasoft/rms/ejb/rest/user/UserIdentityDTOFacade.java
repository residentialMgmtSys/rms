/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.rest.user;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.ejb.exception.LoginException;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import org.javasoft.rms.producers.RestEasyClientProducer;
import org.javasoft.rms.proxy.core.LoginProxy;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.DEFAULT_REST_PATH;
import org.javasoft.rmsLib.utils.error.ErrorDetail;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 *
 * @author ayojava
 */
@Slf4j
@Stateless
@LocalBean
public class UserIdentityDTOFacade {
    
    @Inject
    private RestEasyClientProducer restEasyClientProducer;
    
    private ResteasyWebTarget restEasyWebTarget;
    
    @PostConstruct
    public void init(){
        restEasyWebTarget = restEasyClientProducer.getRestEasyClient().target(UriBuilder.fromPath(DEFAULT_REST_PATH));
    }
    
    public UserIdentityDTO login(String userName,String password) throws LoginException{
        LoginProxy loginProxy = restEasyWebTarget.proxy(LoginProxy.class); 
        Response userIdentityDTOProxy = loginProxy.logIn(userName, password);
        if(userIdentityDTOProxy.getStatusInfo() == Response.Status.OK){
            return userIdentityDTOProxy.readEntity(UserIdentityDTO.class);  
        }else{
            ErrorDetail errorDetail = userIdentityDTOProxy.readEntity(ErrorDetail.class);
            log.info("ErrorDetail ::: {}" ,errorDetail);
            throw new LoginException(errorDetail.getDetail());
        }
    }
    
    public void updateUserIdentityStatus(Long userIdentityId , String status ){
    
    }
}
