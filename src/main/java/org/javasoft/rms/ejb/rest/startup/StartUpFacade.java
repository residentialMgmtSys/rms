/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.rest.startup;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author ayojava
 */
@Slf4j
@Startup
@Singleton
public class StartUpFacade {
    
    @EJB
    private InitDTOFacade initDTOFacade;
        
    @PostConstruct
    public void init(){
        initDTOFacade.loadInitDTO();
    }
}
