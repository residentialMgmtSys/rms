/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.rest.startup;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.bean.utils.DBSelectMenuBean;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsLib.dto.util.InitDTO;
import org.javasoft.rms.producers.RestEasyClientProducer;
import org.javasoft.rms.proxy.core.InitProxy;
import org.javasoft.rms.registry.GlobalRegistry;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import static org.javasoft.rmsLib.utils.FunctionUtils.CONVERT_TO_ESTATEVO;
import static org.javasoft.rmsLib.utils.FunctionUtils.CONVERT_TO_RESIDENTVO;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.DEFAULT_REST_PATH;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 *
 * @author ayojava
 */
@Slf4j
@Stateless
public class InitDTOFacade {

    @Inject
    private RestEasyClientProducer restEasyClientProducer;
   
    @Inject
    private DBSelectMenuBean dbSelectMenuBean;
    
    @Inject
    private GlobalRegistry globalRegistry;
    
    private ResteasyWebTarget restEasyWebTarget;
    
    public void loadInitDTO(){
        restEasyWebTarget = restEasyClientProducer.getRestEasyClient().target(UriBuilder.fromPath(DEFAULT_REST_PATH));
        InitProxy applicationInitProxy = restEasyWebTarget.proxy(InitProxy.class);
        InitDTO initDTO =  applicationInitProxy.loadInitDTO();
        if(initDTO != null && initDTO.isDataAvailable()){
            globalRegistry.setServerStartTime(new Date());
            allocate(initDTO);
        }
    }
    
    private void allocate(InitDTO initDTO){
        setProfileDTOList(initDTO.getProfileDTOs());
        setEstateUnitDTOList(initDTO.getEstateUnitDTOs());
        setResidentDTOList(initDTO.getResidentDTOs());
    }
    
    private void setProfileDTOList(List<ProfileDTO> profileDTOs){
        dbSelectMenuBean.setProfileDTOs(profileDTOs);
    }
    
    private void setEstateUnitDTOList(List<EstateUnitDTO> estateUnitDTOs){
        globalRegistry.setEstateUnitVO(CONVERT_TO_ESTATEVO.apply(estateUnitDTOs));
    }
    
    private void setResidentDTOList(List<ResidentDTO> residentDTOs){
        globalRegistry.setResidentVO(CONVERT_TO_RESIDENTVO.apply(residentDTOs));
    }
}
