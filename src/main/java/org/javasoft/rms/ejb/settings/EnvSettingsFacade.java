/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.settings;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.javasoft.rms.entity.settings.EnvSettings;
import org.javasoft.rms.repository.settings.EnvSettingsRepository;

/**
 *
 * @author ayojava
 */
@Stateless
public class EnvSettingsFacade {

    @Inject
    private EnvSettingsRepository envSettingsRepository;

    public void saveLog(EnvSettings entity){
        envSettingsRepository.save(entity);
    }
    
    public List<EnvSettings> findAll(){
        return envSettingsRepository.findAll();
    }
}
