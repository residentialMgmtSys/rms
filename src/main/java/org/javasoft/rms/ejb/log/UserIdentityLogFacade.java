/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.log;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.javasoft.rms.entity.log.UserIdentityLog;
import org.javasoft.rms.repository.log.UserIdentityLogRepository;

/**
 *
 * @author ayojava
 */
@Stateless
public class UserIdentityLogFacade {
    
    @Inject
    private UserIdentityLogRepository userIdentityLogRepository;

    public void saveLog(UserIdentityLog entity){
        userIdentityLogRepository.save(entity);
    }
    
    public List<UserIdentityLog> findAll(){
        return userIdentityLogRepository.findAll();
    }
}
