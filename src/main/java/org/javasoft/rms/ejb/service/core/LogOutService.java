/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.service.core;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.javasoft.rms.bean.core.UserSessionBean;
import org.javasoft.rms.registry.LoggedInUsersRegistry;

/**
 *
 * @author ayojava
 */
@Stateless
public class LogOutService implements LogOutServiceIntf {

    @Inject
    private LoggedInUsersRegistry loggedInUsersRegistry;
    
    @Inject
    private UserSessionBean userSessionBean;
    
    public void logOut(){
    
    }
}
