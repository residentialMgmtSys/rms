/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.service.core;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.javasoft.rms.bean.core.UserSessionBean;
import org.javasoft.rms.entity.image.ResidentImage;
import org.javasoft.rms.repository.log.ResidentImageRepository;

/**
 *
 * @author ayojava
 */
@Stateless
public class ResidentImageService {

    @Inject
    private UserSessionBean userSessionBean;

    @Inject
    private ResidentImageRepository residentImageRepository;

    public ResidentImage findResidentImage() {
        Long profileId = userSessionBean.getLoggedinProfile().getProfileId();
        Long residentId = userSessionBean.getUserIdentityDTO().getUserIdentityId();
        return residentImageRepository.findByProfileIdAndResidentId(profileId, residentId);
    }
}
