/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.service.dashBoard;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.javasoft.rms.registry.GlobalRegistry;
import org.javasoft.rms.registry.LoggedInUsersRegistry;
import static org.javasoft.rmsLib.utils.ProfilePredicateUtils.ADMIN_PROFILE_PREDICATE;
import static org.javasoft.rmsLib.utils.ProfilePredicateUtils.RESIDENT_PROFILE_PREDICATE;
import org.javasoft.rmsLib.vo.DashboardVO;
import org.javasoft.rmsLib.vo.EstateUnitVO;
import org.javasoft.rmsLib.vo.ResidentVO;

/**
 *
 * @author ayojava
 */
@Stateless
public class DashBoardService {

    @Inject
    private GlobalRegistry globalRegistry;

    @Inject
    private LoggedInUsersRegistry loggedInUsersRegistry;

    public void setDashBoardData(EstateUnitVO estateUnitVO, ResidentVO residentVO, DashboardVO dashboardVO) {
        estateUnitVO = globalRegistry.getEstateUnitVO();
        residentVO = globalRegistry.getResidentVO();
        dashboardVO.setServerStartTime(globalRegistry.getServerStartTime());

        dashboardVO.setLoggedInAdminProfile(loggedInUsersRegistry.getRegistry().entrySet().stream()
                .filter(map -> ADMIN_PROFILE_PREDICATE.test(map.getKey()))
                .flatMap(map -> map.getValue().stream()).count());

        dashboardVO.setLoggedInResidentProfile(loggedInUsersRegistry.getRegistry().entrySet().stream()
                .filter(map -> RESIDENT_PROFILE_PREDICATE.test(map.getKey()))
                .flatMap(map -> map.getValue().stream()).count());
    }
}
