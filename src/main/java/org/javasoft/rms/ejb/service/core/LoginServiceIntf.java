/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.service.core;

import javax.ejb.Local;

/**
 *<div class="input-group input-group-sm"></div>
 * @author ayojava
 */
public interface LoginServiceIntf {

    public boolean login(String userName, String password, Long profileId);
}
