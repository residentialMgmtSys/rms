/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.ejb.service.core;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.bean.core.UserSessionBean;
import org.javasoft.rms.bean.utils.DBSelectMenuBean;
import org.javasoft.rms.bean.utils.MessageUtilBean;
import org.javasoft.rms.ejb.exception.LoginException;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import org.javasoft.rmsLib.dto.user.UserProfileDTO;
import org.javasoft.rms.ejb.rest.user.UserIdentityDTOFacade;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.APPLICATION_ERROR_TEMPLATE;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.INACTIVE_ACCOUNT_ERROR_TEMPLATE;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.LOGIN_SUCCESSFUL_INFO_TEMPLATE;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.NO_AUTHORIZATION_FOR_PROFILE_ERROR_TEMPLATE;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.NO_USER_PROFILE_ERROR_TEMPLATE;
import org.javasoft.rms.registry.LoggedInUsersRegistry;
import static org.javasoft.rmsLib.utils.PredicateUtils.COMPARE_LONG_VALUES;

import org.javasoft.rmsLib.vo.UserVO;
import org.omnifaces.util.Faces;

/**
 *
 * @author ayojava
 */
@Slf4j
@Stateless
@LocalBean
public class LoginService implements LoginServiceIntf {

    @Getter
    @Setter
    private String messageTemplate;

    @EJB
    private UserIdentityDTOFacade userIdentityFacade;

    @Inject
    private UserSessionBean userSessionBean;

    @Inject
    private DBSelectMenuBean dbSelectMenuBean;

    @Inject
    private MessageUtilBean messageUtilBean;

    @Inject
    private HttpSession session;

    @Inject
    private LoggedInUsersRegistry loggedInUsersRegistry;

    private UserIdentityDTO userIdentityDTO;

    @Override
    public boolean login(String userName, String password, Long profileId) {
        boolean allowLogin, profileFound = false;
        try {
            userIdentityDTO = userIdentityFacade.login(userName, password);
            if (userIdentityDTO.isActive()) {
                log.info(" This Account is active  , Login allowed ");
                allowLogin = true;
                if (userIdentityDTO.isSysAdmin()) {
                    log.info(" This Account is a System Admin ");
                    profileFound = true;
                } else {
                    log.info(" This Account is not a System Admin ");
                    List<UserProfileDTO> userProfileDTOs = userIdentityDTO.getUserProfileDTOs();
                    if (userProfileDTOs != null && !userProfileDTOs.isEmpty()) {
                        profileFound = userProfileDTOs.stream().
                                filter((UserProfileDTO userProfileDTO) -> COMPARE_LONG_VALUES.test(userProfileDTO.getProfile().getProfileId(), profileId)).
                                findFirst().isPresent();
                        if (profileFound) {
                            //work on userMenuBean 
                        } else {
                            log.warn(" You are not authorized to Login to the selected profile ");
                            messageUtilBean.displayMessage(NO_AUTHORIZATION_FOR_PROFILE_ERROR_TEMPLATE);
                        }
                    } else {
                        log.error(" No userProfile has been setup , kindly contact the administrator ");
                        messageUtilBean.displayMessage(NO_USER_PROFILE_ERROR_TEMPLATE);
                        profileFound = false;
                    }
                }
            } else {
                allowLogin = false;
                messageUtilBean.displayMessage(INACTIVE_ACCOUNT_ERROR_TEMPLATE);
                log.warn("This account is {} , Login not permitted", userIdentityDTO.getDTOFlagName());
            }
            if (allowLogin && profileFound) {
                updateSessionDetails(profileId);
                updateRegistryDetails();
                messageUtilBean.displayMessage(LOGIN_SUCCESSFUL_INFO_TEMPLATE);
            }
        } catch (LoginException ex) {
            allowLogin = false;
            messageUtilBean.setViewMsg(ex.getMessage());
        } catch (Exception ex) {
            allowLogin = false;
            messageUtilBean.displayMessage(APPLICATION_ERROR_TEMPLATE);
            log.error("Exception Message ::::", ex);
        }
        return (allowLogin && profileFound);
    }

    private void updateSessionDetails(Long profileId) {
        userIdentityDTO.setLoggedIn();
        userSessionBean.setUserIdentityDTO(userIdentityDTO);
        Optional<ProfileDTO> optProfileDTO = dbSelectMenuBean.getProfileDTOs().stream().
                filter(profileDTO -> COMPARE_LONG_VALUES.test(profileDTO.getProfileId(), profileId)).findAny();
        userSessionBean.setLoggedinProfile(optProfileDTO.get());
        userSessionBean.setEventIP(Faces.getRemoteAddr());
        userSessionBean.setSessionID(session.getId());
    }

    private void updateRegistryDetails() {
        UserVO userVO = new UserVO();
        userVO.setFullName(userIdentityDTO.getResidentDetails().getFullName());
        userVO.setLocale("");
        userVO.setLoginDateTime(LocalDateTime.now());
        userVO.setProfileCode(userSessionBean.getLoggedinProfile().getProfileCode());
        userVO.setProfileName(userSessionBean.getLoggedinProfile().getProfileName());
        userVO.setUserId(userIdentityDTO.getUserIdentityId());
        userVO.setUserName(userIdentityDTO.getUserName());
        loggedInUsersRegistry.registerUserVO(userVO);
    }

}
