/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import static org.javasoft.rmsLib.utils.PredicateUtils.COMPARE_LONG_VALUES;
import org.javasoft.rmsLib.vo.UserVO;

/**
 *
 * @author ayojava
 */
@Slf4j
public class LoggedInUsersRegistry {

    @Getter
    private HashMap<String, List<UserVO>> registry;

    public void init() {
        registry = new HashMap<>();
    }

    public void registerUserVO(UserVO userVO) {

        List<UserVO> userVOList = registry.compute(userVO.getProfileCode(), (String key, List<UserVO> value) -> {
            return (value == null || value.isEmpty()) ? new ArrayList<>() : value;
        });

        userVOList.removeIf(aUserVO -> (COMPARE_LONG_VALUES.test(userVO.getUserId(), aUserVO.getUserId())));
        userVOList.add(userVO);
    }

    public void unRegisterUserVO(String profileCode , Long userID) {

        List<UserVO> userVOList = registry.compute(profileCode, (String key, List<UserVO> value) -> {
            return value; });
        boolean output = userVOList.removeIf(aUserVo -> (COMPARE_LONG_VALUES.test(aUserVo.getUserId(), userID)));
        log.info("Unregister User ::::: {}", output);
    }
}
