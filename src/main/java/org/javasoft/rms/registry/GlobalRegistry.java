/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.registry;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import org.javasoft.rmsLib.vo.EstateUnitVO;
import org.javasoft.rmsLib.vo.ResidentVO;

/**
 *
 * @author ayojava
 */
public class GlobalRegistry {
    
    @Getter @Setter
    private EstateUnitVO estateUnitVO;
    
    @Getter @Setter
    private ResidentVO residentVO;
    
    @Getter @Setter
    private Date serverStartTime;
}
