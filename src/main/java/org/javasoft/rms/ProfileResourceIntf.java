/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms;

/**
 *
 * @author ayojava
 */
public interface ProfileResourceIntf {
    
    String DEFAULT_INCLUDE_PATH = "/WEB-INF/includes/";

    String PAGE_EXTENSION = ".xhtml";

    String DEFAULT_MENU_PATH = "menu/content";

    String DEFAULT_DASHBOARD_PATH = "dashboard/summary-dashboard";
        
    String VIEW_HOME_PAGE = "view-home-page";
    
    int DASHBOARD_REFRESH_INTERVAL = 10;
}
