/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.exception;

/**
 *
 * @author ayojava
 */
public class SystemStartUpException extends IllegalStateException{
    
    public SystemStartUpException(String message){
        super(message);
    }
    
}