/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.i18n.msgs;

/**
 *
 * @author ayojava
 */
public interface RmsViewMsgs {

    String PROPERTY_FILE = "org.javasoft.rms.i18n.msgs.rmsViewMsgs";

    String APPLICATION_ERROR_TEMPLATE = "{application.error}";

    String NO_USER_PROFILE_ERROR_TEMPLATE = "{noUserProfile.error}";

    String NO_AUTHORIZATION_FOR_PROFILE_ERROR_TEMPLATE = "{noAuthorizationForProfile.error}";

    String ALREADY_LOGGED_IN_ERROR_TEMPLATE = "{alreadyLoggedIn.error}";

    String INACTIVE_ACCOUNT_ERROR_TEMPLATE = "{inactiveAccount.error}";

    String LOGIN_SUCCESSFUL_INFO_TEMPLATE = "{loginSuccessful.info}";

    String LOCKED_ACCOUNT_ERROR_TEMPLATE = "{lockedAccount.error}";

    String UNAUTHORIZED_ACCESS_ERROR_TEMPLATE = "{unauthorizedAccess.error}";

    String INVALID_LOGIN_DETAILS_ERROR_TEMPLATE = "{invalidLoginDetails.error}";

    
    String SAVE_OPERATION_SUCCESSFUL_TEMPLATE = "{saveOperation.success}";
    
    String SAVE_OPERATION_FAILED_TEMPLATE = "{saveOperation.failed}";
    
    String EDIT_OPERATION_SUCCESSFUL_TEMPLATE = "{editOperation.success}";
    
    String EDIT_OPERATION_FAILED_TEMPLATE = "{editOperation.failed}";
    
    
    String APPROVAL_OPERATION_SUCCESSFUL_TEMPLATE = "{approvalOperation.success}";
    
    String APPROVAL_OPERATION_FAILED_TEMPLATE = "{approvalOperation.failed}";
    

    String REJECT_OPERATION_SUCCESSFUL_TEMPLATE = "{rejectOperation.success}";
    
    String REJECT_OPERATION_FAILED_TEMPLATE = "{rejectOperation.failed}";
    
    
    String SAVE_APPROVAL_OPERATION_SUCCESSFUL_TEMPLATE = "{saveApprovalOperation.success}";
    
    String SAVE_APPROVAL_OPERATION_FAILED_TEMPLATE = "{saveApprovalOperation.failed}";
    
    
    String EDIT_APPROVAL_OPERATION_SUCCESSFUL_TEMPLATE = "{editApprovalOperation.success}";
    
    String EDIT_APPROVAL_OPERATION_FAILED_TEMPLATE = "{editApprovalOperation.failed}";
    

    String USER_ACCOUNT_ACTIVATED_TEMPLATE = "{userAccountActivated.success}";

    String DATA_ALREADY_EXISTS_WARNING_TEMPLATE = "{dataAlreadyExists.warning}";

    String NO_SELECTION_WARNING_TEMPLATE = "{noSelection.warning}";

    String NO_PROFILE_SELECTED_WARNING_TEMPLATE = "{noProfileSelected.warning}";

    String UPLOAD_OPERATION_SUCCESSFUL_TEMPLATE = "{uploadOperation.success}";

    String UPLOAD_OPERATION_FAILED_TEMPLATE = "{uploadOperation.failed}";

    String IMAGE_DELETE_OPERATION_SUCCESSFUL_TEMPLATE = "{imageDeleteOperation.success}";

    String VALIDATE_OPERATION_FAILED_TEMPLATE = "{validateOperation.failed}";

    String VALIDATE_CURRENT_PASSWORD_FAILED_TEMPLATE = "{validateCurrentPassword.warning}";

    String VALIDATE_SAME_PASSWORD_USERNAME_WARNING_TEMPLATE = "{validateSamePasswordUsername.warning}";

    String VALIDATE_MINIMUM_PASSWORD_LENGTH_WARNING_TEMPLATE = "{validateMinimumPasswordLength.warning}";

    String NEW_PASSWORD_AND_CONFIRM_PASSWORD_WARNING_TEMPLATE = "{newPasswordAndConfirmPassword.warning}";

    String VALIDATE_INVALID_PASSWORD_FORMAT_WARNING_TEMPLATE = "{validateInvalidPasswordFormat.warning}";
    
    String CHANGE_LOGIN_DETAILS_SUCCESS_TEMPLATE = "{changeLoginDetails.success}";
}
