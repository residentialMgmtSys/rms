/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.admin;

import org.javasoft.rms.ProfileResourceIntf;

/**
 *
 * @author ayojava
 */
public interface AdminProfileResourceIntf extends ProfileResourceIntf{
    
    String ADMIN_DASHBOARD_PATH = "dashboard/admin-summary-dashboard";
    
    String ADMIN_URL = "admin/";
}
