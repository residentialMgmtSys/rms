/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.admin;

/**
 *
 * @author ayojava
 */
public interface AdminMenuIntf {
    
    /*=========== MODULES ===========*/
    String LIST_MODULES = "list-modules";

    String NEW_MODULE = "new-module";

    String EDIT_MODULE = "edit-module";

    String VIEW_MODULE = "view-module";

    /*=========== MENUS ===========*/
    String LIST_MENUS = "list-menus";

    String NEW_MENU = "new-menu";

    String EDIT_MENU = "edit-menu";

    String VIEW_MENU = "view-menu";

    /*=========== RESIDENTIAL FACILITY ===========*/
    String LIST_RESIDENTIAL_UNITS = "list-residential-units";

    String NEW_RESIDENTIAL_UNIT = "new-residential-unit";

    String EDIT_RESIDENTIAL_UNIT = "edit-residential-unit";

    String VIEW_RESIDENTIAL_UNIT = "view-residential-unit";
    
    String VIEW_RESIDENTIAL_UNIT_APPROVAL = "view-residential-unit-approval";
    
    String LIST_RESIDENTIAL_UNIT_APPROVALS = "list-residential-unit-approvals";
    
    /*=========== ESTATE LEVY ===========*/
    String LIST_ESTATE_LEVIES = "list-estate-levies";

    String NEW_ESTATE_LEVY = "new-estate-levy";

    String EDIT_ESTATE_LEVY = "edit-estate-levy";

    String VIEW_ESTATE_LEVY = "view-estate-levy";

    /*=========== RESIDENT-FACILITY  ===========*/
    String NEW_REAL_RESIDENT_RESIDENTIAL_UNIT = "new-real-resident-residential-unit";

    /*=========== RESIDENTIAL LOG ===========*/
    String LIST_FACILITY_LOGS = "list-facility-logs";
    
    String NEW_FACILITY_LOG = "new-facility-log";
    
    /*=========== NON-RESIDENTIAL FACILITY ===========*/
    String LIST_NON_RESIDENTIAL_UNITS = "list-non-residential-units";

    String NEW_NON_RESIDENTIAL_UNIT = "new-non-residential-unit";

    String EDIT_NON_RESIDENTIAL_UNIT = "edit-non-residential-unit";

    String VIEW_NON_RESIDENTIAL_UNIT = "view-non-residential-unit";

    /*=========== NON-RESIDENTIAL LOG ===========*/
    String LIST_NON_RESIDENTIAL_UNIT_LOGS = "list-non-residential-unit-logs";

    String NEW_NON_RESIDENTIAL_UNIT_LOG = "new-non-residential-unit-log";

    String EDIT_NON_RESIDENTIAL_UNIT_LOG = "edit-non-residential-unit-log";

    String VIEW_NON_RESIDENTIAL_UNIT_LOG = "view-non-residential-unit-log";

    /*=========== REAL-RESIDENT===========*/
    String LIST_REAL_RESIDENTS = "list-real-residents";

    String NEW_REAL_RESIDENT = "new-real-resident";

    String EDIT_REAL_RESIDENT = "edit-real-resident";

    String VIEW_REAL_RESIDENT = "view-real-resident";
    
    String VIEW_REAL_RESIDENT_APPROVAL = "view-real-resident-approval";
    
    String LIST_REAL_RESIDENT_APPROVALS = "list-real-resident-approvals";

    /*=========== VIRTUAL-RESIDENT===========*/
    String LIST_VIRTUAL_RESIDENTS = "list-virtual-residents";

    String NEW_VIRTUAL_RESIDENT = "new-virtual-resident";

    String EDIT_VIRTUAL_RESIDENT = "edit-virtual-resident";

    String VIEW_VIRTUAL_RESIDENT = "view-virtual-resident";

    /*===========RESIDENT_ACCOUNT===========*/
    String VIEW_RESIDENT_ACCOUNT = "view-resident-account";

    /*=========== USERS===========*/
    String LIST_SYSTEM_USERS = "list-system-users";

    String EDIT_SYSTEM_USER = "edit-system-user";

    String VIEW_SYSTEM_USER = "view-system-user";
    
    String ACTIVATE_SYSTEM_USER = "activate-system-user";

    String SYSTEM_USER_LOG = "system-user-log";
    
    /*=========== PROFILE===========*/
    String EDIT_PROFILE = "edit-profile";

    String VIEW_PROFILE = "view-profile";
    
    String PROFILE_LOG = "profile-log";
    
    String CHANGE_LOGIN_DETAILS = "change-login-details";
    
    /*=========== ENVSETTINGS===========*/
    
    String EDIT_ENV_SETTING = "edit-env-setting";
    
    String VIEW_ENV_SETTING = "view-env-setting";
     
    /*=========== REAL-RESIDENT-GROUP ===========*/
    
    String LIST_REAL_RESIDENT_GROUPS = "list-real-resident-groups";

    String NEW_REAL_RESIDENT_GROUP = "new-real-resident-group";

    String EDIT_REAL_RESIDENT_GROUP = "edit-real-resident-group";
    
    String DELETE_REAL_RESIDENT_GROUP = "delete-real-resident-group";

    String VIEW_REAL_RESIDENT_GROUP = "view-real-resident-group";
}
