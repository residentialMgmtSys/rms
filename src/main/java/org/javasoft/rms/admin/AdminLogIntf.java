/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.admin;

/**
 *
 * @author ayojava
 */
public interface AdminLogIntf {
    
    String LOGGED_IN_EVENT = "Logged In";

    String LOGGED_OUT_EVENT = "Logged Out";

    String NEW_MODULE_EVENT = "Added a Module";

    String EDIT_MODULE_EVENT = "Edited a Module";

    String NEW_MENU_EVENT = "Added a Menu";

    String EDIT_MENU_EVENT = "Edited a Menu";
    
    String EDIT_PROFILE_EVENT = "Edited User Profile";
    
    String NEW_RESIDENTIAL_UNIT_EVENT = "Added a Residential Unit";
    
    String EDIT_LOGIN_DETAILS_EVENT = "Edited Log in Details";
    
    String USER_ACCOUNT_CREATED_EVENT= "User Account Created";
    
    String USER_ACCOUNT_ACTIVATED_EVENT = "User Account Activated";
    
    String NEW_REAL_RESIDENT_EVENT = "Added A New Real Resident";
    
    String NEW_ESTATE_LEVY_EVENT = "Added A New Estate Levy";
    
    String APPROVE_RESIDENTIAL_UNIT_LOG_EVENT = "Approve Residential Unit";
    
    String REJECT_RESIDENTIAL_UNIT_LOG_EVENT = "Reject Residential Unit";
    
    String APPROVE_REAL_RESIDENT_LOG_EVENT = "Approve Real Resident ";
    
    String REJECT_REAL_RESIDENT_LOG_EVENT= "Reject Real Resident ";
}
