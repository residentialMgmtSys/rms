/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.bean.utils.EventUtilBean;

/**
 *
 * @author ayojava
 */
@Slf4j
@Named("logOutBean")
@RequestScoped
public class LogOutBean {
    
    @Inject
    private EventUtilBean eventUtilBean;
    
    public void logOut(){
        
        eventUtilBean.fireUserIdentityLogEvent("");
    }
}
