/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;

/**
 *
 * @author ayojava
 */
@Slf4j
@Named("userSessionBean")
@SessionScoped
public class UserSessionBean  implements Serializable{
    
    @Getter @Setter
    private ProfileDTO loggedinProfile;
    
    @Getter @Setter
    private UserIdentityDTO userIdentityDTO;
    
    @Getter @Setter
    private String eventIP;
    
    @Getter @Setter
    private String sessionID;
    
    @PostConstruct
    public void init(){
    
    }
}
