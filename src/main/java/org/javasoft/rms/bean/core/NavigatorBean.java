/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import java.io.File;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import static org.javasoft.rms.ProfileResourceIntf.DEFAULT_DASHBOARD_PATH;
import static org.javasoft.rms.ProfileResourceIntf.DEFAULT_INCLUDE_PATH;
import static org.javasoft.rms.ProfileResourceIntf.DEFAULT_MENU_PATH;
import static org.javasoft.rms.ProfileResourceIntf.PAGE_EXTENSION;
import static org.javasoft.rms.admin.AdminProfileResourceIntf.ADMIN_DASHBOARD_PATH;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;

/**
 *
 * @author ayojava
 */
@Named("navigatorBean")
@SessionScoped
public class NavigatorBean implements Serializable{
    
    private String selectedPagePath;

    private String selectedMenuPath;
    
    private ProfileDTO loggedInProfile;
    
    private String profileName;
    
    private boolean sysAdmin;
    
    @Inject
    private UserSessionBean userSessionBean;
    
    @PostConstruct
    public void init(){
        loggedInProfile = userSessionBean.getLoggedinProfile();
        profileName = loggedInProfile.getProfileName().toLowerCase()+ "-profile";
        sysAdmin = userSessionBean.getUserIdentityDTO().isSysAdmin();
    }
    
    public void setProfileHomePath() { 
        StringBuilder builder = new StringBuilder();
        builder = builder.append(DEFAULT_INCLUDE_PATH).append(profileName).append(File.separator);
        builder =builder.append((loggedInProfile.isAdminProfile() && sysAdmin) ? ADMIN_DASHBOARD_PATH :DEFAULT_DASHBOARD_PATH);
        builder = builder.append(PAGE_EXTENSION);
        setSelectedPagePath(builder.toString());
    }
    
    public void setPagePath(String pagePath){
        StringBuilder builder = new StringBuilder();
        builder = builder.append(DEFAULT_INCLUDE_PATH).append(profileName).append(File.separator).append(pagePath).append(PAGE_EXTENSION);
        setSelectedPagePath(builder.toString());
    }
    
    public String getSelectedPagePath() {
        if (StringUtils.isBlank(selectedPagePath)) {
            setProfileHomePath();
        }
        return selectedPagePath;
    }

    public String getSelectedMenuPath() {
        if (StringUtils.isBlank(selectedMenuPath)) {
            StringBuilder builder = new StringBuilder();
            builder = builder.append(DEFAULT_INCLUDE_PATH).append(profileName).append(File.separator).append(DEFAULT_MENU_PATH).append(PAGE_EXTENSION);
            selectedMenuPath = builder.toString();
        }
        return selectedMenuPath;
    }

    public void setSelectedMenuPath(String selectedMenuPath) {
        this.selectedMenuPath = selectedMenuPath;
    }

    public void setSelectedPagePath(String selectedPagePath) {
        this.selectedPagePath = selectedPagePath;
    }
}
