/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.ejb.service.core.ResidentImageService;
import org.javasoft.rms.entity.image.ResidentImage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author ayojava
 */
@Slf4j
@SessionScoped
@Named("profileImageBean")
public class ProfileImageBean implements Serializable{
    
    @Getter @Setter
    private boolean imageFlag;
    
    private ResidentImage residentImage;
    
    private StreamedContent streamedImage;
    
    @EJB
    private ResidentImageService residentImageService;
    
    @PostConstruct
    public void init() {
        checkResidentImage();
    }
    
    public void checkResidentImage() {
       residentImage = residentImageService.findResidentImage();
       setImageFlag(residentImage==null);
    }
    
    public StreamedContent getStreamedImage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            streamedImage = new DefaultStreamedContent();
        } else {
            if (residentImage != null) {
                streamedImage = new DefaultStreamedContent(new ByteArrayInputStream(residentImage.getContent()), residentImage.getContentType());
            } else {
                streamedImage = new DefaultStreamedContent();
            }
        }
        return streamedImage;
    }
    
    @PreDestroy
    public void destroy() {
        residentImage = null;
        streamedImage = null;
    }
}
