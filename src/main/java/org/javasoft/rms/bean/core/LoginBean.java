/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import static org.javasoft.rms.admin.AdminLogIntf.LOGGED_IN_EVENT;
import org.javasoft.rms.bean.utils.EventUtilBean;
import org.javasoft.rms.bean.utils.MessageUtilBean;
import org.javasoft.rms.ejb.service.core.LoginService;
import org.omnifaces.util.Messages;

/**
 * Have another layer which handles the actual rest calls 
 * @author ayojava
 */
@Slf4j
@RequestScoped
@Named("loginBean")
public class LoginBean {
    
    @Getter   @Setter
    private String userName;

    @Getter   @Setter
    private String password;

    @Getter   @Setter
    private Long profileId;
    
    @EJB
    private LoginService loginService;
    
    @Inject
    private MessageUtilBean messageUtilBean;
    
    @Inject
    private EventUtilBean eventUtilBean;
            
    @PostConstruct
    public void init() {
        
    }
    
    public String login() {
        log.info("Username :: {}", userName);
        log.info("Password :: {}", password);
        log.info("ProfileId :: {}", profileId);
        
        if(loginService.login(userName, password, profileId)){
            Messages.addFlashGlobalInfo(messageUtilBean.getViewMsg());
            eventUtilBean.fireUserIdentityLogEvent(LOGGED_IN_EVENT);      
            return "home?faces-redirect=true";
        }
        Messages.addGlobalError(messageUtilBean.getViewMsg());
        return null;
    }
}
