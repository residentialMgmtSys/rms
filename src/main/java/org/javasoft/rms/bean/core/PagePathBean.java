/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.core;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.PagePathIntf;

/**
 *
 * @author ayojava
 */
@Slf4j
@Named
@RequestScoped
public  class PagePathBean implements PagePathIntf{
    
    @Inject
    private NavigatorBean navigatorBean;
    
    @Override
    public void setPagePath(String pagePath) {
        navigatorBean.setPagePath(pagePath);
    }
    
    public void setProfileHomePath(){
        navigatorBean.setProfileHomePath();
    }
}
