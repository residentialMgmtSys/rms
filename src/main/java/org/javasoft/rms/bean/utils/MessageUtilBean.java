/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.utils;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.deltaspike.core.api.message.Message;
import org.apache.deltaspike.core.api.message.MessageContext;
import org.javasoft.rms.MessageIntf;
import static org.javasoft.rms.i18n.msgs.RmsViewMsgs.PROPERTY_FILE;

/**
 *
 * @author ayojava
 */
@Slf4j
@RequestScoped
@Named("messageUtilBean")
public class MessageUtilBean implements MessageIntf{
    
    @Inject
    private MessageContext messageContext;
    
    @Getter @Setter
    private String viewMsg;
    
    @Override
    public void displayMessage(String template, String... args) {
        Message message = messageContext.messageSource(PROPERTY_FILE).message();
        viewMsg = message.template(template).argument((Serializable[])args).toString();
    }
    
    @Override
    public void displayMessage(String template) {
        Message message = messageContext.messageSource(PROPERTY_FILE).message();
        viewMsg = message.template(template).toString();
    }

    
}
