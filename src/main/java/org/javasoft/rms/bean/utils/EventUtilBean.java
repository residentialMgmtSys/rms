/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.bean.events.EventBeanIntf;
import org.javasoft.rms.bean.events.EventEnum;
import org.javasoft.rms.qualifiers.EventType;

/**
 *
 * @author ayojava
 */
@Slf4j
@RequestScoped
@Named("eventUtilBean")
public class EventUtilBean {
    
    @Inject
    @EventType(type = EventEnum.UserIdentityLog)
    private EventBeanIntf eventBeanIntf;

    public void fireUserIdentityLogEvent(String eventType) {
        eventBeanIntf.fireEvent(eventType);
    }
}
