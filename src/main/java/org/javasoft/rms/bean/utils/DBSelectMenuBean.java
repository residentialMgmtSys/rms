/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.utils;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;

/**
 *
 * @author ayojava
 */
@Slf4j
@ApplicationScoped
@Named("dbSelectMenuBean")
public class DBSelectMenuBean implements Serializable{
    
    @Getter @Setter
    private List<ProfileDTO> profileDTOs;
    
    @PreDestroy
    public void destroy() {
        profileDTOs = null;
    }
}
