/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.admin.dashBoard;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.javasoft.rms.PagePathIntf;
import org.javasoft.rms.bean.core.PagePathBean;
import org.javasoft.rms.ejb.service.dashBoard.DashBoardService;
import static org.javasoft.rms.ProfileResourceIntf.VIEW_HOME_PAGE;
import org.javasoft.rmsLib.vo.DashboardVO;
import org.javasoft.rmsLib.vo.EstateUnitVO;
import org.javasoft.rmsLib.vo.ResidentVO;

/**
 *
 * @author ayojava
 */
@Slf4j
@RequestScoped
@Named("dashBoardPageBean")
public class DashBoardPageBean  implements PagePathIntf{
        
    @Inject
    private PagePathBean pagePathBean;
            
    @EJB
    private DashBoardService dashBoardService;
    
    @Getter
    private EstateUnitVO estateUnitVO;
    
    @Getter
    private ResidentVO residentVO;
    
    @Getter
    private DashboardVO dashboardVO;
    
    @PostConstruct
    public void init() {
        showData();
    }

    @Override
    public void setPagePath(String pagePath) {
        if (StringUtils.equals(VIEW_HOME_PAGE, pagePath)) {
            pagePathBean.setProfileHomePath();
        }
    }
     
    public void showData() {
        dashboardVO = new DashboardVO();
        dashBoardService.setDashBoardData(estateUnitVO, residentVO, dashboardVO);
    }
    
    
}
