/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.observers;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.ejb.log.UserIdentityLogFacade;
import org.javasoft.rms.entity.log.UserIdentityLog;

/**
 *
 * @author ayojava
 */
@Slf4j
@Named
@RequestScoped
public class UserIdentityLogObserverBean {

    @EJB
    private UserIdentityLogFacade userIdentityLogFacade;

    public void userIdentityLogEvent(@Observes UserIdentityLog userIdentityLog) {
        
        userIdentityLogFacade.saveLog(userIdentityLog);
    }
}
