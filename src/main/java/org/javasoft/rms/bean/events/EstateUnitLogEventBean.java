/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.bean.events;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rms.entity.log.EstateUnitLog;
import org.javasoft.rms.qualifiers.EventType;

/**
 *
 * @author ayojava
 */
@Slf4j
@Named
@RequestScoped
@EventType(type = EventEnum.EstateUnitLog)
public class EstateUnitLogEventBean implements EventBeanIntf{
    
    @Inject
    Event<EstateUnitLog> estateUnitLogEvent;
    
    @Inject
    private EstateUnitLog estateUnitLog;
    
    @Override
    public void fireEvent(String eventTitle){
        estateUnitLog.setLogTitle(eventTitle);
        estateUnitLogEvent.fire(estateUnitLog);
    }
}
