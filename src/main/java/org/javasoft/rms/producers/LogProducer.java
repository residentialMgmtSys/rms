/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.producers;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import org.javasoft.rms.bean.core.UserSessionBean;
import org.javasoft.rms.entity.log.AbstractLog;
import org.javasoft.rms.entity.log.EstateUnitLog;
import org.javasoft.rms.entity.log.UserIdentityLog;

/**
 *
 * @author ayojava
 */
@Dependent
public class LogProducer {

    @Produces
    @RequestScoped
    @Named("userIdentityLog")
    public UserIdentityLog getUserIdentityLog(UserSessionBean userSessionBean) {
        UserIdentityLog userIdentityLog = new UserIdentityLog();
        userIdentityLog.setEventIP(userSessionBean.getEventIP());
        userIdentityLog.setProfileCode(userSessionBean.getLoggedinProfile().getProfileCode());
        userIdentityLog.setProfileId(userSessionBean.getLoggedinProfile().getProfileId());
        userIdentityLog.setUserIdentityId(userSessionBean.getUserIdentityDTO().getUserIdentityId());
        userIdentityLog.setUserSystemNo(userSessionBean.getUserIdentityDTO().getSystemNo());
        userIdentityLog.setResidentId(userSessionBean.getUserIdentityDTO().getResidentDetails().getResidentId());
        userIdentityLog.setResidentName(userSessionBean.getUserIdentityDTO().getResidentDetails().getFullName());
        return userIdentityLog;
    }

    @Produces
    @RequestScoped
    @Named("estateUnitLog")
    public EstateUnitLog getEstateUnitLog(UserSessionBean userSessionBean) {
        EstateUnitLog estateUnitLog = new EstateUnitLog();
        estateUnitLog.setEventIP(userSessionBean.getEventIP());
        estateUnitLog.setProfileCode(userSessionBean.getLoggedinProfile().getProfileCode());
        estateUnitLog.setProfileId(userSessionBean.getLoggedinProfile().getProfileId());
        estateUnitLog.setUserIdentityId(userSessionBean.getUserIdentityDTO().getUserIdentityId());
        estateUnitLog.setUserSystemNo(userSessionBean.getUserIdentityDTO().getSystemNo());
        estateUnitLog.setResidentId(userSessionBean.getUserIdentityDTO().getResidentDetails().getResidentId());
        estateUnitLog.setResidentName(userSessionBean.getUserIdentityDTO().getResidentDetails().getFullName());
        return estateUnitLog;
    }
    
}
