/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.producers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import org.javasoft.rms.registry.EnvRegistry;
import org.javasoft.rms.registry.GlobalRegistry;
import org.javasoft.rms.registry.LoggedInUsersRegistry;

/**
 *
 * @author ayojava
 */
@Dependent
public class RegistryProducer {
    
    @Produces
    @ApplicationScoped
    public GlobalRegistry globalRegistry(){
        return new GlobalRegistry();
    }
    
    @Produces
    @ApplicationScoped
    public EnvRegistry envRegistry(){
        return new EnvRegistry();
    }
    
    @Produces
    @ApplicationScoped
    public LoggedInUsersRegistry loggedInUsersRegistry(){
        LoggedInUsersRegistry registry = new LoggedInUsersRegistry();
        registry.init();
        return registry;
    }
}
