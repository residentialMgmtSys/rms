/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.producers;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;

/**
 *
 * @author ayojava 
 * 
 */
@Slf4j
@ApplicationScoped
public class RestEasyClientProducer {
    
    @Getter
    private ResteasyClient restEasyClient;

    @PostConstruct
    public void initResteasyClient() {
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        CloseableHttpClient closableHttpClient = HttpClients.custom().setConnectionManager(poolingHttpClientConnectionManager).build();
        poolingHttpClientConnectionManager.setMaxTotal(200); // Increase max total connection to 200
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(20); // Increase default max connection per route to 20
        ApacheHttpClient4Engine apacheHttpClient4Engine = new ApacheHttpClient4Engine(closableHttpClient);
        restEasyClient =  new ResteasyClientBuilder().httpEngine(apacheHttpClient4Engine).build();
    }
}
/*
@Slf4j
@Dependent
public class RestEasyClientProducer  {
       
    @Produces
    @ApplicationScoped
    @RestEasyClientQualifier
    public ResteasyClient gettResteasyClient(){
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        CloseableHttpClient closableHttpClient = HttpClients.custom().setConnectionManager(poolingHttpClientConnectionManager).build();
        poolingHttpClientConnectionManager.setMaxTotal(200); // Increase max total connection to 200
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(20); // Increase default max connection per route to 20
        ApacheHttpClient4Engine apacheHttpClient4Engine = new ApacheHttpClient4Engine(closableHttpClient);
        
        return new ResteasyClientBuilder().httpEngine(apacheHttpClient4Engine).build();       
    }
    
    
}
 */
