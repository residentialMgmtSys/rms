/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.producers;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ayojava
 */
@Slf4j
@Dependent
public class DatabaseProducer {
    /*
    @PersistenceContext(unitName = "rmswebPU")
    private EntityManager em;
    */
    
    @PersistenceUnit
    private EntityManagerFactory emf;
    
    //@mysqlDB
    @Produces
    //@TransactionScoped
    //@Default
    @RequestScoped
    public EntityManager create(){
        return emf.createEntityManager();
    }

    public void close(@Disposes EntityManager em) {
        if (em.isOpen()) {
            em.close();
        }
    }
    
//    public void close(@Disposes @mysqlDB EntityManager em) {
//        if (em.isOpen()) {
//            em.close();
//        }
//    }
}
