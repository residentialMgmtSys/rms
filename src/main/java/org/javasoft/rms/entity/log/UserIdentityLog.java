/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.entity.log;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ayojava
 */
@Data
@Entity
public class UserIdentityLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "UserIdentityLog_GEN", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "UserIdentityLog_GEN", sequenceName = "UserIdentityLog_SEQ")
    private Long logId;

    @NotEmpty
    private String eventIP;

    @NotEmpty
    private String logTitle;

    @NotEmpty
    @Column(nullable = false)
    private String residentName;

    @NotEmpty
    @Column(nullable = false)
    private Long residentId;

    @Lob
    private String logDescription;

    @NotEmpty
    @Column(nullable = false)
    private Long userIdentityId;

    @NotEmpty
    @Column(nullable = false)
    private String userSystemNo;

    @NotEmpty
    @Column(nullable = false)
    private Long profileId;

    @NotEmpty
    @Column(nullable = false)
    private String profileCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date createDate;

}
