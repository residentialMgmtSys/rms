/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rms.entity.image;

import java.io.Serializable;
import javax.persistence.Entity;
import lombok.Data;

/**
 *
 * @author ayojava
 */
@Data
@Entity
public class ResidentImage extends AbstractImage implements Serializable {

    private Long residentId;
    
}
